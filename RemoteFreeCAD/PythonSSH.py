# coding=utf-8
'''
Copyright © 2018 Diego Ariel Capeletti

This file is part the proyect RemoteFreeCAD.

RemoteFreeCAD is libre software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RemoteFreeCAD Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with RemoteFreeCAD.  If not, see <http://www.gnu.org/licenses/>.
'''

'''
Created on 24 jun. 2018

@author: dcapeletti
Script para crear un tunel ssh para conexión segura.
Deberá reemplazar ip o servidor por los datos que corresponda.
'''

from sshtunnel import SSHTunnelForwarder
from time import sleep
import subprocess
import platform

with SSHTunnelForwarder(
    ('ip o servidor', 2299),
    ssh_username=raw_input("Ingrese usuario:\n") ,
    ssh_password=raw_input("Ingrese contrasena:\n"),
    remote_bind_address=('127.0.0.1', 445)
) as server:

    print(server.local_bind_port) #Imprimo el puerto
    if platform.system() == "Linux":
        #subprocess.call(["mount -t cifs -o <username>,<password> //<servername>/<sharename> /var/point/"])
        subprocess.call(["nautilus", "-s", "smb://localhost:" + str(server.local_bind_port)])
    else: #plataforma Winbugs
        subprocess.call(["explorer", "file://localhost:" + str(server.local_bind_port)])
    
    while True:
        # press Ctrl-C for stopping
        sleep(1)

print('FINISH!')
'''
python -m sshtunnel -U dcapeletti -P password -L :2020 -R 127.0.0.1:445 -p 2299 ip o servidor
'''

