# coding=utf-8
'''
Copyright © 2018 Diego Ariel Capeletti

This file is part the proyect RemoteFreeCAD.

RemoteFreeCAD is libre software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RemoteFreeCAD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with RemoteFreeCAD Server.  If not, see <http://www.gnu.org/licenses/>.
'''


'''
Created on 17 jun. 2018

@author: dcapeletti
La clase RemoteFreeCAD permite crear una conexión a una instancia de
freecad. Abrir FreeCAD y en el terminal python escribir:

import Web
Web.startServer() #Se puede especificar un puerto si es necesario.

Ahora FreeCAD esta listo para recibir comandos remotamente a través de
la instancia de clase RemoteFreeCAD.
'''

import socket
import threading
#import socketserver
import time



#ip = "localhost"
#port=43445 # see above
class RemoteFreeCAD:
    def __init__(self, hostFreeCAD, port):
        self.host = hostFreeCAD
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((self.host, port))
        
    def send(self, message):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.host, self.port))
        try:
           sock.sendall(message.encode())
           response = sock.recv(1024)
           return response
        finally:
           sock.close()
            
    def executeMacro(self, macroName):
        self.send("execfile(App.ParamGet('User parameter:BaseApp/Preferences/Macro').GetString('MacroPath', App.getUserAppDataDir())+'" + macroName + ".FCMacro')")
            
    def crearDocumentoNuevo(self):
        message =  "import FreeCAD\nFreeCAD.newDocument()"
        try:
            self.sock.sendall(message.encode())
            response = self.sock.recv(1024)
            print ("Received: {}".format(response))
        finally:
            #self.sock.close()
            print ("dsad")

            
    def crearNuevoCubo(self, name, size=(10, 10, 10)):
        #self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #self.sock.connect((self.host, self.port))
        message =  []
        message.append("App.ActiveDocument.addObject('Part::Box','" + name + "')")
        message.append("FreeCAD.getDocument('Unnamed').getObject('" + name + "').Length = '" + str(size[0]) + " mm'")
        message.append("FreeCAD.getDocument('Unnamed').getObject('" + name + "').Width = '" + str(size[1]) + " mm'")
        message.append("FreeCAD.getDocument('Unnamed').getObject('" + name + "').Height = '" + str(size[2]) + " mm'")
        message.append("App.ActiveDocument.recompute()")
        message.append("Gui.SendMsgToActiveView('ViewFit')")
        #self.send("App.ActiveDocument.addObject('Part::Box','Box')")
        #self.send("App.ActiveDocument.recompute()")
        #self.send("Gui.SendMsgToActiveView('ViewFit')")
        #self.send("Gui.activeDocument().activeView().viewAxonometric()")
        for val in message:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.connect((self.host, self.port))
            try:
                self.sock.sendall(val.encode())
                response = self.sock.recv(1024)
                print ("Received: {}".format(response))
            finally:
                print ("sd")
        return name
    
    
    
    def cambiarTransparencia(self, obj, transparencia=0):
        self.send("FreeCADGui.getDocument('Unnamed').getObject('"+ str(obj) +"').Transparency = "+ str(transparencia))
        
        
    def transparenciaAleatoria(self, obj):
        x=0
        while True:
            self.cambiarTransparencia(obj, transparencia=x)
            x=x+1
            time.sleep(0.1)   
            if x==100:
                x=0   
   
    def cambiarBancoDeTrabajo(self, banco):
        self.send('Gui.activateWorkbench("'+ banco +'")')
            
#import time            
#client = RemoteFreeCAD("localhost", 44123)
#client.createNewDocument()
#time.sleep(5)
#cubo = send.createNewBox("ds", size=(50, 30, 30))
#client.transparenciaAleatoria(cubo)
#client.changeTransparency(cubo, 90)


