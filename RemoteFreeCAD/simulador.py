import time

class Simulador:
    def __init__(self, conexion):
        self.conexion = conexion

    def vaciar_tanque(self):
        self.conexion.send("App.ActiveDocument.Variables.tiempo=0")
        self.conexion.send("App.activeDocument().recompute(None,True,True)")

    def leer_sensor(self, dato):
        if dato > 60 or dato < 0:
            raise ValueError("Dato incorrecto. Los valores van entre 0 y 60")
        self.conexion.send("App.ActiveDocument.Variables.tiempo=" + str(dato))
        self.conexion.send("App.activeDocument().recompute(None,True,True)")

    def simular_llenando_tanque(self):
        valor = 0
        while True:
            #print("LLenando el tanque")
            if valor >= 61:
                return "El tanque se ha llenado..."
            self.conexion.send("App.ActiveDocument.Variables.tiempo="+str(valor))
            self.conexion.send("App.activeDocument().recompute(None,True,True)")
            valor+=1
            time.sleep(0.5)